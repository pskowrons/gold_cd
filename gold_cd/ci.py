from http import HTTPStatus

from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Project
from gitlab.v4.objects import ProjectJob
from gitlab.v4.objects import ProjectPipeline

__all__ = ["color_status", "get_pipeline", "ref2sha"]


def color_status(status: str) -> str:
    match status:
        case "success":
            return f"[green]{status}[/green]"
        case "failed":
            return f"[red]{status}[/red]"
        case "running":
            return f"[yellow]{status}[/yellow]"
        case "pending":
            return f"[magenta]{status}[/magenta]"
        case "created":
            return f"[cyan]{status}[/cyan]"
        case "manual":
            return f"[blue]{status}[/blue]"
        case "skipped":
            return f"[orange3]{status}[/orange3]"
        case "canceled":
            return f"[dark_orange]{status}[/dark_orange]"
        case _:
            return status


def get_pipeline(project: Project, job_or_pipeline: int) -> ProjectPipeline | None:
    """
    Get project pipeline from pipeline or job id.
    """
    try:
        pipeline: ProjectPipeline = project.pipelines.get(job_or_pipeline)
    except GitlabGetError as exc:
        if exc.response_code == HTTPStatus.NOT_FOUND:
            try:
                job: ProjectJob = project.jobs.get(job_or_pipeline)
                pipeline = project.pipelines.get(job.pipeline["id"])
            except GitlabGetError as job_exc:
                if job_exc.response_code == HTTPStatus.NOT_FOUND:
                    return None
                raise job_exc
        else:
            raise exc
    return pipeline


def ref2sha(project: Project, ref: str) -> str:
    if ref.startswith("refs/tags/"):
        tag = project.tags.get(ref[10:])
        return tag.attributes["commit"]["id"]

    if ref.startswith("refs/heads/"):
        branch = project.branches.get(ref[11:])
        return branch.attributes["commit"]["id"]

    try:
        tag = project.tags.get(ref)
        return tag.attributes["commit"]["id"]
    except GitlabGetError:
        ...

    try:
        branch = project.branches.get(ref)
        return branch.attributes["commit"]["id"]
    except GitlabGetError:
        ...

    try:
        commit = project.commits.get(ref)
        return commit.id
    except GitlabGetError:
        ...
    raise Exception(f"{project.path_with_namespace}: unknown ref [{ref}]")
