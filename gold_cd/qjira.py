import json
import logging
from json.decoder import JSONDecodeError
from pathlib import Path

from jira import JIRA
from jira.exceptions import JIRAError

__all__ = ["QJIRA", "AuthenticationError"]


class AuthenticationError(Exception):
    pass


class QJIRA(JIRA):
    JIRA_SERVER = "https://qorvo.atlassian.net"
    JIRA_CREDENTIAL = Path.home() / ".jira-credentials"

    def __init__(self, credentials: Path = JIRA_CREDENTIAL, *args, **kwargs):
        try:
            basic_auth = json.loads(credentials.read_text(encoding="UTF-8"))
        except FileNotFoundError as exc:
            logging.error(
                "%s: %s [%d]",
                exc.filename,
                exc.strerror,
                exc.errno,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            raise AuthenticationError from exc
        except JSONDecodeError as exc:
            logging.error(
                "%s: format error at %d:%d [%s]",
                exc.filename,
                exc.lineno,
                exc.colno,
                exc.msg,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            raise AuthenticationError from exc
        super().__init__(
            server=self.JIRA_SERVER, basic_auth=basic_auth, *args, **kwargs
        )
        try:
            _ = self.session()
        except JIRAError as exc:
            logging.error(
                "jira: %s [HTTP %d]",
                exc.text,
                exc.status_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            raise AuthenticationError from exc

    def summary(self, key: str) -> str:
        try:
            issue = self.issue(key)
        except JIRAError as exc:
            logging.error(
                "%s: %s [HTTP %d]",
                key,
                exc.text,
                exc.status_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            return ""
        return issue.get_field("summary")
