from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO

from .manifest import Manifest

__all__ = ["WestManifest"]


class MyYAML(YAML):
    def dump(self, data, stream=None, **kw):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        YAML.dump(self, data, stream, **kw)
        if inefficient:
            return stream.getvalue()


class WestManifest(Manifest):
    """manages the west manifest file"""

    def update_projects_revision(self, revision: str) -> str:
        yaml = MyYAML()
        manifest = yaml.load(self._content)
        for project in manifest["manifest"]["projects"]:
            name = project["name"]
            if "groups" not in self.projects[name]:
                continue
            if "release" not in self.projects[name]["groups"]:
                continue
            if "revision" in project:
                project["revision"] = revision
            else:
                manifest["defaults"]["revision"] = revision
        yaml.indent(mapping=2, sequence=4, offset=2)
        return yaml.dump(manifest)

    def parse(self, manifests: dict | None = None):
        if self.projects is not None:
            return
        yaml = YAML()
        manifest = yaml.load(self._content)["manifest"]
        if "remotes" in manifest:
            self._remotes = {
                remote["name"]: remote["url-base"] for remote in manifest["remotes"]
            }
        self.projects = {}
        for project in manifest["projects"]:
            name = project["name"]
            if "default" in manifest:
                project = manifest["default"] | project
            if "import" in project:
                Exception(f'Error: cannot handle "import" for {name}')
            if "url" in project:
                url = project["url"]
            elif "remote" in project:
                remote_url = self._remotes[project["remote"]]
                if "repo-path" in project:
                    url = f'{remote_url}/{project["repo-path"]}'
                else:
                    url = f"{remote_url}/{name}"
            else:
                Exception(f"Error: cannot determine url for {name}")
            self.projects[name] = {
                "ref": (project["revision"] if "revision" in project else "master"),
                "url": url,
            }
            if "groups" in project:
                self.projects[name]["groups"] = project["groups"]
