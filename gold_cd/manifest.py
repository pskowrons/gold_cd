import logging
from abc import ABC
from abc import abstractmethod

from gitlab.v4.objects import Project

__all__ = ["Manifest"]


class Manifest(ABC):
    """Abstract class for manifests"""

    def __init__(
        self,
        path: str,
        content: bytes,
        project: Project,
    ):
        logging.info(
            "Manifest:\n\t- project: %s,\n\t- path: %s,\n\t- content:\n%s",
            project.path_with_namespace,
            path,
            content.decode(),
        )
        self.path = path
        self._content = content
        self._repo = project
        self.projects = None

    @abstractmethod
    def update_projects_revision(self, revision: str) -> str: ...

    @abstractmethod
    def parse(self, manifests: dict | None = None): ...
