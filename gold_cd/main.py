import io
import logging
import sys
import tempfile
import time
from datetime import datetime
from fnmatch import fnmatch
from functools import update_wrapper
from importlib.metadata import version as metadata_version
from pathlib import Path
from typing import Callable, cast
from urllib.parse import urlparse
from zipfile import ZipFile

import rich.text
import rich_click as click
from gitlab import Gitlab
from gitlab.exceptions import GitlabCreateError, GitlabDeleteError, GitlabGetError
from gitlab.v4.objects import (
    Project,
    ProjectJob,
    ProjectPipeline,
    ProjectPipelineBridge,
    ProjectPipelineJob,
)
from rich.console import Console
from rich.logging import RichHandler
from rich.progress import Progress, SpinnerColumn, TextColumn, TimeElapsedColumn
from rich.table import Column
from rich.text import Text

from gold_cd.ci import color_status, get_pipeline, ref2sha
from gold_cd.qjira import QJIRA
from gold_cd.repo import RepoManifest
from gold_cd.west import WestManifest

__author__ = "Philippe Skowronski"
__copyright__ = "Copyright \N{COPYRIGHT SIGN} 2024, Philippe Skowronski"

__license__ = "GPL"
__version__ = metadata_version("gold_cd")
__maintainer__ = "Philippe Skowronski"
__email__ = "philippe.skowronski@qorvo.com"

LOGLEVELS = [
    logging.CRITICAL,
    logging.ERROR,
    logging.WARNING,
    logging.INFO,
    logging.DEBUG,
]

CONTEXT_SETTINGS = {
    "help_option_names": ["-h", "--help"],
    "token_normalize_func": lambda x: x.lower(),
}
click.rich_click.OPTION_GROUPS = {
    "gold-cd release": [
        {
            "name": "Basic options",
            "options": [
                "--ref",
                "--individual",
                "--issue",
                "--commit",
            ],
        },
        {
            "name": "Advanced options",
            "options": ["--help", "--force"],
        },
    ],
}
click.rich_click.USE_RICH_MARKUP = True

TITLE_LENGTH = 50

ZEPHYR_MANIFEST = "qorvo/uwb-mobile/qm/firmware/zephyr-manifest"

ANDROID_GROUP = "qorvo/uwb-mobile/qm/android"
AOSP_MANIFEST = f"{ANDROID_GROUP}/aosp-manifest"
ANDROID_CI = f"{ANDROID_GROUP}/ci"

TOOLS_GROUP = "qorvo/uwb-mobile/tools"
TOOLS = {
    "CAT": f"{TOOLS_GROUP}/test/cat/cat-campaign",
    "UQT": f"{TOOLS_GROUP}/uci/uwb-qorvo-tools",
}

SDK = {
    "DOC": "qorvo/uwb-iot/proj/apps-projects/documentation",
    "QNF": "qorvo/tech/uwb/sdk/qnf",
    "FLASH": "qorvo/tech/uwb/host/flashing/qm-flashing",
}


def configure_logging(
    debug: int,
    log_time_format: str | Callable[[datetime], rich.text.Text],
):
    from rich.traceback import install

    loglevels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
    loglevel: int
    try:
        loglevel = loglevels[debug]
    except IndexError:
        loglevel = loglevels[-1]
    logging.basicConfig(
        level=loglevel,
        format="%(message)s",
        handlers=[
            RichHandler(
                omit_repeated_times=False,
                show_path=False,
                enable_link_path=False,
                markup=True,
                rich_tracebacks=True,
                log_time_format=log_time_format,
            ),
        ],
    )
    install(suppress=[click])


def pass_obj(f):
    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        return ctx.invoke(f, ctx.obj, *args, **kwargs)

    return update_wrapper(new_func, f)


def individual2sha(gl: Gitlab, individual: list[tuple[str, str]]) -> dict[str, str]:
    result = {}
    for path, ref in individual:
        project = gl.projects.get(path)
        result[path] = ref2sha(project, ref)
    return result


def wait_for_job(job: ProjectJob, console: Console, width=TITLE_LENGTH) -> str:
    """Wait for a job to end."""
    end_states = ["success", "failed", "canceled", "skipped"]
    if job.status not in end_states:
        with Progress(
            TextColumn(
                "[progress.description]{task.description}",
                table_column=Column(width=width),
            ),
            SpinnerColumn(),
            TimeElapsedColumn(
                table_column=Column(justify="right", width=10),
            ),
            console=console,
            transient=True,
        ) as progress:
            _ = progress.add_task(
                description=(
                    f"Waiting for job [cyan]{job.id}[/cyan]"
                    f" ([magenta]{job.name}[/magenta]) to end"
                ),
                total=None,
            )
            while (
                job.status not in end_states
                and job.pipeline["status"] not in end_states
            ):
                time.sleep(5)
                job.refresh()

    console.log(
        f"Job {job.id} ([magenta]{job.name}[/magenta])"
        f" status: {color_status(job.status)}"
    )

    match job.status:
        case "skipped" | "failed" | "canceled":
            sys.exit(1)
        case "success" | "running":
            ...
        case _:
            raise Exception(f"Unexpected job status: {job.status}")

    return job.status


def ci_get_repo_manifest_sha(pipeline: ProjectPipeline, console: Console) -> str:
    """Get REPO_MANIFEST_SHA from 'rev-locked-manifest: ' job traces."""
    gl = pipeline.manager.gitlab
    project = gl.projects.get(pipeline.project_id)
    for job in pipeline.jobs.list(iterator=True):
        if not job.name.startswith("rev-locked-manifest:"):
            continue
        job = project.jobs.get(job.id)
        wait_for_job(job, console)
        logging.debug("Parse '%s' job (%d) traces", job.name, job.id)
        for line in cast(bytes, job.trace()).decode().split("\n"):
            logging.debug(Text.from_ansi(line))
            if not line.startswith("REPO_MANIFEST_SHA="):
                continue
            return line[18:]
        break
    raise Exception(f"Cannot retrieve REPO_MANIFEST_SHA value from job {job.id}")


def exists_release_branch(
    project: Project, branch: str, force: bool, dry_run: bool, console: Console
):
    if not force:
        return

    try:
        prj_branch = project.branches.get(branch)
    except GitlabGetError:
        return

    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: delete branch [green]{prj_branch.name}[/]"
            f" on [magenta]{project.path_with_namespace}[/]"
        )
        return

    console.log(
        f"[magenta]{project.path_with_namespace}[/]:"
        f" branch [green]{prj_branch.name}[/] to delete"
    )
    try:
        prj_branch.delete()
        console.log(
            f"[magenta]{project.path_with_namespace}[/]:"
            f" branch [green]{prj_branch.name}[/] deleted"
        )
    except GitlabDeleteError as exc:
        console.log(
            f"[magenta]{project.path_with_namespace}[/]:"
            f" branch [green]{prj_branch.name}[/] cannot be deleted"
            f" [{exc.error_message}]"
        )


def create_release_branch(
    project: Project,
    branch: str,
    ref: str,
    force: bool,
    dry_run: bool,
    console: Console,
):
    exists_release_branch(project, branch, force, dry_run, console)

    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create branch [green]{branch}[/]"
            f" on [magenta]{project.path_with_namespace}[/]"
        )
        return

    try:
        prj_branch = project.branches.create(
            dict(
                branch=branch,
                ref=ref,
            )
        )
        console.log(
            f"[magenta]{project.path_with_namespace}[/]:"
            f" branch [green]{prj_branch.name}[/] created on [cyan]{ref}[/]"
        )
    except GitlabCreateError as exc:
        logging.warning(
            "%s: %s [HTTP %d]",
            project.path_with_namespace,
            exc.error_message,
            exc.response_code,
            extra={
                # "markup": True,
                "highlighter": None,
            },
        )


@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(
    __version__,
    "-v",
    "--version",
    message=f"%(prog)s, version %(version)s\n{__copyright__}",
)
@click.option("--debug", "-d", count=True, help="Debug mode")
@click.option(
    "--just-print",
    "--dry-run",
    "--recon",
    "-n",
    "dry_run",
    is_flag=True,
    default=False,
    show_default=True,
    help="Print the commands that would be executed, but do not execute them",
)
@click.pass_obj
def cli(obj, debug: int, dry_run: bool):
    localtz = datetime.now().astimezone().strftime("%z")
    log_time_format = f"[%Y-%m-%d %H:%M:%S.%f{localtz}]"
    configure_logging(debug, log_time_format)
    obj["dry-run"] = dry_run
    obj["console"] = Console(
        color_system="truecolor",
        log_path=False,
        log_time_format=log_time_format,
    )


@cli.group(context_settings=CONTEXT_SETTINGS, chain=True)
@click.pass_obj
@click.option(
    "--force",
    "-f",
    is_flag=True,
    default=False,
    show_default=True,
    help="Override existing tag",
)
@click.option("--issue", type=str, help="Jira ticket")
@click.argument("tag")
def tag(
    obj,
    issue: str | None,
    tag: str,
    force: bool,
):
    """Create release branch on all repositories and update manifest files"""
    gl = Gitlab.from_config()
    gl.auth()

    obj |= dict(
        issue=issue,
        tag=tag,
        force=force,
        gl=gl,
    )


@tag.command("fw", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
@click.option(
    "--repository",
    default=ZEPHYR_MANIFEST,
    show_default=True,
    type=str,
    help="fw manifest repository path with namespace",
)
@click.option(
    "--pipeline",
    "pipeline_id",
    type=int,
    help="retrieve pipeline frozen manifest",
)
def fw_tag(
    obj,
    repository: str,
    pipeline_id: int,
):
    """Create tag on all firmware repositories"""
    console = obj["console"]
    gl = obj["gl"]
    tag = obj["tag"]
    # force = obj["force"]
    dry_run = obj["dry-run"]

    try:
        project = gl.projects.get(repository)
    except GitlabGetError as exc:
        logging.error(
            "%s: %s [HTTP %d]",
            repository,
            exc.error_message,
            exc.response_code,
            extra={
                # "markup": True,
                "highlighter": None,
            },
        )
        sys.exit(1)

    pipeline = get_pipeline(project, pipeline_id)
    if pipeline is None:
        logging.error("Unknown pipeline id: %d", pipeline_id)
        sys.exit(1)
    ref = pipeline.sha

    logging.info("%s: ref=%s", project.path_with_namespace, ref)
    content = project.files.raw(
        file_path="west.yml",
        ref=ref,
    )
    manifest = WestManifest("west.yml", content, project)
    manifest.parse()

    logging.info("Download pipeline [link=URL]%s[/link]", pipeline.web_url)
    for job in pipeline.jobs.list(iterator=True):
        if job.name != "prepare":
            continue

        job = project.jobs.get(job.id)
        wait_for_job(job, console)

        for artifact in job.attributes["artifacts"]:
            if artifact["file_type"] == "archive":
                break
        else:
            logging.warning("Job %d (%s) artifact not available", job.id, job.name)
            continue

        zip = ZipFile(io.BytesIO(job.artifacts()))
        for file in zip.infolist():
            if fnmatch(Path(file.filename).name, "zephyr-manifest-*-*.yml"):
                break
            if fnmatch(
                Path(file.filename).name, "west-zephyr-manifest-prepare-*-*.yml"
            ):
                break
        else:
            logging.error(
                "pipeline %d: job prepare %d has no known manifest file",
                job.pipeline["id"],
                job.id,
            )
            sys.exit(1)
        local_manifest = zip.extract(file, tempfile.mkdtemp())

        break

    with Path(local_manifest).open("rb") as reader:
        content = reader.read()
    override_manifest = WestManifest("west.yml", content, project)
    override_manifest.parse()
    for prj in override_manifest.projects:
        if prj not in manifest.projects:
            continue
        if "groups" not in manifest.projects[prj]:
            continue
        if "release" not in manifest.projects[prj]["groups"]:
            continue
        manifest.projects[prj]["ref"] = override_manifest.projects[prj]["ref"]

    release_projects = {
        prj["url"]: prj["ref"]
        for prj in manifest.projects.values()
        if "groups" in prj and "release" in prj["groups"]
    }
    if not release_projects:
        console.log("Projects to release empty: nothing to do")
        return

    console.log("Projects to release:", release_projects)
    for url, prj_ref in release_projects.items():
        prj_path = urlparse(url).path[1:]
        if prj_path.endswith(".git"):
            prj_path = prj_path[:-4]
        if prj_path == repository:
            continue
        prj = gl.projects.get(prj_path)
        if dry_run:
            console.log(
                f"[orange_red1]TODO[/]: create tag {tag}"
                f" in [magenta]{prj.path_with_namespace}[/]"
                f" on sha [green]{prj_ref}[/]"
            )
        else:
            try:
                tag_obj = prj.tags.create(dict(tag_name=tag, ref=prj_ref))
                console.log(
                    f"tag {tag_obj.name} created"
                    f" in [magenta]{prj.path_with_namespace}[/]"
                    f" on sha [green]{tag_obj.commit['id']}[/]"
                )
            except GitlabCreateError as exc:
                logging.warning(
                    "%s: %s [HTTP %d]",
                    project.path_with_namespace,
                    exc.error_message,
                    exc.response_code,
                    extra={
                        # "markup": True,
                        "highlighter": None,
                    },
                )

    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create tag {tag}"
            f" in [magenta]{project.path_with_namespace}[/]"
            f" on sha [green]{ref}[/]"
        )
    else:
        try:
            tag_obj = project.tags.create(dict(tag_name=tag, ref=ref))
            console.log(
                f"tag {tag_obj.name} created"
                f" in [magenta]{project.path_with_namespace}[/]"
                f" on sha [green]{tag_obj.commit['id']}[/]"
            )
        except GitlabCreateError as exc:
            logging.warning(
                "%s: %s [HTTP %d]",
                project.path_with_namespace,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )


@tag.command("aosp", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
@click.option(
    "--repository",
    default=AOSP_MANIFEST,
    show_default=True,
    type=str,
    help="AOSP manifest repository path with namespace",
)
@click.option(
    "--pipeline",
    "pipeline_id",
    type=int,
    help="retrieve pipeline frozen manifest",
)
def aosp_tag(
    obj,
    repository: str,
    pipeline_id: int,
):
    """Create tag on all AOSP repositories"""
    console = obj["console"]
    gl = obj["gl"]
    tag = obj["tag"]
    # force = obj["force"]
    dry_run = obj["dry-run"]

    try:
        asop_manifest_project: Project = gl.projects.get(repository)
    except GitlabGetError as exc:
        sys.exit(f"{repository}: {exc.error_message} [HTTP {exc.response_code}]")

    try:
        ci_project: Project = gl.projects.get(ANDROID_CI)
    except GitlabGetError as exc:
        sys.exit(f"{ANDROID_CI}: {exc.error_message} [HTTP {exc.response_code}]")

    # Get ci downstream pipeline id from AOSP manifest pipeline
    aosp_pipeline = get_pipeline(asop_manifest_project, pipeline_id)
    if aosp_pipeline is not None:
        pipeline_bridge: ProjectPipelineBridge
        for pipeline_bridge in aosp_pipeline.bridges.list(iterator=True):
            if pipeline_bridge.downstream_pipeline["project_id"] == ci_project.id:
                pipeline_id = pipeline_bridge.downstream_pipeline["id"]
                break
        else:
            sys.exit(f"Cannot find ci downstream pipeline for pipeline {pipeline_id}")

    ci_pipeline = get_pipeline(ci_project, pipeline_id)
    if ci_pipeline is None:
        sys.exit(f"Error: unknown pipeline id: {pipeline_id}")
    ref = ci_get_repo_manifest_sha(ci_pipeline, console)

    # Get all Qorvo manifest
    manifests = {}
    try:
        for node in asop_manifest_project.repository_tree(
            recursive=True,
            ref=ref,
            iterator=True,
        ):
            if not node["type"] == "blob":
                continue
            if not node["name"].endswith("manifest-qorvo.xml"):
                continue
            console.log(f'Found manifest [green]{node["path"]}[/]')
            content = asop_manifest_project.repository_raw_blob(node["id"])
            manifest = RepoManifest(node["path"], content, asop_manifest_project)
            manifests[node["path"]] = manifest
    except GitlabGetError as exc:
        sys.exit(f"{repository}: {exc.error_message} [HTTP {exc.response_code}]")

    # Parse manifest
    # Must be parsed all together to handle 'include' tag
    for manifest in manifests.values():
        manifest.parse(manifests)

    logging.info(
        "Download rev-locked-manifest jobs pipeline [link=URL]%s[/link]",
        ci_pipeline.web_url,
    )
    override_manifests = {}
    pipeline_job: ProjectPipelineJob
    for pipeline_job in ci_pipeline.jobs.list(iterator=True):
        if not pipeline_job.name.startswith("rev-locked-manifest:"):
            continue

        project_job: ProjectJob = ci_project.jobs.get(pipeline_job.id)
        wait_for_job(project_job, console)
        xmlfile = project_job.name[22:-1]
        logging.info("Job %d: manifest=[magenta]%s[/magenta]", project_job.id, xmlfile)
        if xmlfile not in manifests:
            logging.info("Skipping manifest=[magenta]%s[/magenta]", xmlfile)
            continue

        # for artifact in project_job.attributes["artifacts"]:
        #     if artifact["file_type"] == "archive":
        #         break
        # else:
        #     logging.warning(
        #         "Job %d (%s) artifact not available",
        #         project_job.id,
        #         project_job.name,
        #     )
        #     continue
        # try:
        #     zip = ZipFile(io.BytesIO(project_job.artifacts()))
        # except GitlabGetError as exc:
        #     logging.warning(
        #         "Job %d (%s) artifact: %s",
        #         project_job.id,
        #         project_job.name,
        #         exc.error_message,
        #     )
        #     continue
        # for file in zip.infolist():
        #     if Path(file.filename).name == xmlfile:
        #         break
        # else:
        #     logging.error(
        #         "pipeline %d: job prepare %d has no locked manifest file",
        #         project_job.pipeline["id"],
        #         project_job.id,
        #     )
        #     sys.exit(1)
        #
        # local_manifest = zip.extract(file, tempfile.mkdtemp())
        # logging.info(
        #     "Job %d: downloaded manifest=[magenta]%s[/magenta]",
        #     project_job.id,
        #     local_manifest,
        # )
        # with Path(local_manifest).open("rb") as reader:
        #     content = reader.read()
        try:
            content = project_job.artifact(
                f"artifacts/locked-manifest-{project_job.id}/{xmlfile}"
            )
        except GitlabGetError as exc:
            sys.exit(
                f"Job {project_job.id} ({project_job.name}) artifact: {exc.error_message}"
            )
        override_manifests[xmlfile] = RepoManifest(
            xmlfile, content, asop_manifest_project
        )

    for manifest in override_manifests.values():
        manifest.parse(override_manifests)

    all_projects = {}
    for manifest in override_manifests.values():
        all_projects |= manifest.projects
    console.log("override_manifest", all_projects)

    for manifest in manifests.values():
        xmlfile = manifest.path
        if xmlfile not in override_manifests:
            continue
        override_manifest = override_manifests[xmlfile]
        for prj in manifest.projects:
            if prj not in override_manifest.projects:
                continue
            manifest.projects[prj]["ref"] = override_manifest.projects[prj]["ref"]

    release_projects = {}
    for manifest in manifests.values():
        for prj in manifest.projects.values():
            if "groups" not in prj:
                continue
            if "release" not in prj["groups"]:
                continue
            url = prj["url"]
            prj_ref = prj["ref"]
            if url in release_projects:
                if prj_ref != release_projects[url]:
                    logging.warning(
                        "[magenta]%s[/]: different revisions"
                        " [cyan]%s[/] vs. [cyan]%s[/]",
                        urlparse(url).path[1:],
                        release_projects[url],
                        prj_ref,
                    )
                    continue
            release_projects[url] = prj_ref
    if not release_projects:
        console.log("Projects to release empty: nothing to do")
        return

    console.log("Projects to release:", release_projects)
    for url, prj_ref in release_projects.items():
        prj_path = urlparse(url).path[1:].removesuffix(".git")
        if prj_path == repository:
            continue
        prj = gl.projects.get(prj_path)
        if dry_run:
            console.log(
                f"[orange_red1]TODO[/]: create tag {tag}"
                f" in [magenta]{prj.path_with_namespace}[/]"
                f" on sha [green]{prj_ref}[/]"
            )
        else:
            try:
                tag_obj = prj.tags.create(dict(tag_name=tag, ref=prj_ref))
                console.log(
                    f"tag {tag_obj.name} created"
                    f" in [magenta]{prj.path_with_namespace}[/]"
                    f" on sha [green]{tag_obj.commit['id']}[/]"
                )
            except GitlabCreateError as exc:
                logging.warning(
                    "%s: %s [HTTP %d]",
                    asop_manifest_project.path_with_namespace,
                    exc.error_message,
                    exc.response_code,
                    extra={
                        # "markup": True,
                        "highlighter": None,
                    },
                )

    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create tag {tag}"
            f" in [magenta]{asop_manifest_project.path_with_namespace}[/]"
            f" on sha [green]{ref}[/]"
        )
    else:
        try:
            tag_obj = asop_manifest_project.tags.create(dict(tag_name=tag, ref=ref))
            console.log(
                f"tag {tag_obj.name} created"
                f" in [magenta]{asop_manifest_project.path_with_namespace}[/]"
                f" on sha [green]{tag_obj.commit['id']}[/]"
            )
        except GitlabCreateError as exc:
            logging.warning(
                "%s: %s [HTTP %d]",
                asop_manifest_project.path_with_namespace,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )

    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create tag {tag}"
            f" in [magenta]{ci_project.path_with_namespace}[/]"
            f" on sha [green]{ci_pipeline.sha}[/]"
        )
    else:
        try:
            tag_obj = ci_project.tags.create(dict(tag_name=tag, ref=ci_pipeline.sha))
            console.log(
                f"tag {tag_obj.name} created"
                f" in [magenta]{ci_project.path_with_namespace}[/]"
                f" on sha [green]{tag_obj.commit['id']}[/]"
            )
        except GitlabCreateError as exc:
            logging.warning(
                "%s: %s [HTTP %d]",
                ci_project.path_with_namespace,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )


@cli.group(context_settings=CONTEXT_SETTINGS, chain=True)
@click.pass_obj
@click.option(
    "--force",
    "-f",
    is_flag=True,
    default=False,
    show_default=True,
    help="Override existing destination branch",
)
@click.option(
    "--ref",
    type=str,
    help="repository source reference [dim][default: repository default branch][/]",
)
@click.option(
    "--individual",
    multiple=True,
    type=(str, str),
    help="specify specific version for a repo format: <repo path> <version>",
)
@click.option("--issue", required=True, type=str, help="Jira ticket")
@click.option(
    "--commit",
    type=str,
    help="commit message [dim][default: retrieve from Atlassian server][/]",
)
@click.argument("release_branch")
def release(
    obj,
    ref: str | None,
    individual: list[tuple[str, str]],
    issue: str,
    commit: str | None,
    release_branch: str,
    force: bool,
):
    """Create release branch on all repositories and update manifest files"""
    gl = Gitlab.from_config()
    gl.auth()

    if not commit:
        jira = QJIRA()
        commit = jira.summary(issue)

    sha = individual2sha(gl, individual)
    obj |= dict(
        ref=ref,
        sha=sha,
        issue=issue,
        commit=commit,
        release_branch=release_branch,
        force=force,
        gl=gl,
    )


@release.command("fw", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
@click.option(
    "--repository",
    default=ZEPHYR_MANIFEST,
    show_default=True,
    type=str,
    help="fw manifest repository path with namespace",
)
@click.option(
    "--manifest",
    "local_manifest",
    type=str,
    help="provide a west.yml file to start from",
)
@click.option(
    "--pipeline",
    "pipeline_id",
    type=int,
    help="retrieve pipeline frozen manifest",
)
def fw_release(
    obj, repository: str, local_manifest: str | None, pipeline_id: int | None
):
    """Create release branch on all firmware repositories and update manifest files"""
    console = obj["console"]
    gl = obj["gl"]
    sha = obj["sha"]
    ref = obj["ref"]
    release_branch = obj["release_branch"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    try:
        project = gl.projects.get(repository)
    except GitlabGetError as exc:
        logging.error(
            "%s: %s [HTTP %d]",
            repository,
            exc.error_message,
            exc.response_code,
            extra={
                # "markup": True,
                "highlighter": None,
            },
        )
        sys.exit(1)

    if not ref:
        ref = project.default_branch

    if pipeline_id:
        pipeline = get_pipeline(project, pipeline_id)
        if pipeline is None:
            logging.error("Unknown pipeline id: %d", pipeline_id)
            sys.exit(1)
        ref = pipeline.sha
    else:
        ref = ref2sha(project, ref)

    logging.info("%s: ref=%s", project.path_with_namespace, ref)
    content = project.files.raw(
        file_path="west.yml",
        ref=sha.get(project.path_with_namespace, ref),
    )
    manifest = WestManifest("west.yml", content, project)
    manifest.parse()

    if pipeline_id:
        logging.info("Download pipeline [link=URL]%s[/link]", pipeline.web_url)
        for job in pipeline.jobs.list(iterator=True):
            if job.name != "prepare":
                continue

            job = project.jobs.get(job.id)
            wait_for_job(job, console)

            for artifact in job.attributes["artifacts"]:
                if artifact["file_type"] == "archive":
                    break
            else:
                logging.warn("Job %d (%s) artifact not available", job.id, job.name)
                continue

            zip = ZipFile(io.BytesIO(job.artifacts()))
            for file in zip.infolist():
                if fnmatch(Path(file.filename).name, "zephyr-manifest-*-*.yml"):
                    break
                if fnmatch(
                    Path(file.filename).name, "west-zephyr-manifest-prepare-*-*.yml"
                ):
                    break
            else:
                logging.error(
                    "pipeline %d: job prepare %d has no manifest",
                    job.pipeline["id"],
                    job.id,
                )
                sys.exit(1)
            local_manifest = zip.extract(file, tempfile.mkdtemp())

    if local_manifest:
        with Path(local_manifest).open("rb") as reader:
            content = reader.read()
        override_manifest = WestManifest("west.yml", content, project)
        override_manifest.parse()
        for prj in override_manifest.projects:
            if prj not in manifest.projects:
                continue
            if "groups" not in manifest.projects[prj]:
                continue
            if "release" not in manifest.projects[prj]["groups"]:
                continue
            manifest.projects[prj]["ref"] = override_manifest.projects[prj]["ref"]

    release_projects = {
        prj["url"]: prj["ref"]
        for prj in manifest.projects.values()
        if "groups" in prj and "release" in prj["groups"]
    }
    if not release_projects:
        console.log("Projects to release empty: nothing to do")
        return

    console.log("Projects to release:", release_projects)
    for url, prj_ref in release_projects.items():
        prj_path = urlparse(url).path[1:]
        if prj_path.endswith(".git"):
            prj_path = prj_path[:-4]
        if prj_path == repository:
            continue
        prj = gl.projects.get(prj_path)
        create_release_branch(
            prj, release_branch, sha.get(prj_path, prj_ref), force, dry_run, console
        )

    exists_release_branch(project, release_branch, force, dry_run, console)
    data = {
        "branch": release_branch,
        "start_sha": sha.get(repository, ref),
        "commit_message": f"{obj['issue']} | {obj['commit']}",
        "actions": [
            {
                "action": "update",
                "file_path": "west.yml",
                "content": manifest.update_projects_revision(release_branch),
            },
        ],
    }
    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create commit in [magenta]{project.path_with_namespace}[/]"
            f" on branch [green]{release_branch}[/] to update west.yml"
        )
    else:
        try:
            commit_obj = project.commits.create(data)
            console.log(f"[link=URL]{commit_obj.web_url}[/link]")
        except GitlabCreateError as exc:
            logging.warning(
                "%s: %s [HTTP %d]",
                project.path_with_namespace,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )


@release.command("aosp", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
@click.option(
    "--repository",
    default=AOSP_MANIFEST,
    show_default=True,
    type=str,
    help="AOSP manifest repository path with namespace",
)
@click.option(
    "--pipeline",
    "pipeline_id",
    type=int,
    help="retrieve pipeline frozen manifest",
)
def aosp_release(obj, repository: str, pipeline_id: int | None):
    """Create release branch on all AOSP repositories and update manifest files"""
    console = obj["console"]
    gl = obj["gl"]
    sha = obj["sha"]
    ref = obj["ref"]
    release_branch = obj["release_branch"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    try:
        project = gl.projects.get(repository)
    except GitlabGetError as exc:
        logging.error(
            "%s: %s [HTTP %d]",
            repository,
            exc.error_message,
            exc.response_code,
            extra={
                # "markup": True,
                "highlighter": None,
            },
        )
        sys.exit(1)

    if not ref:
        ref = project.default_branch

    if pipeline_id:
        aosp_pipeline = get_pipeline(project, pipeline_id)
        if aosp_pipeline:
            pipeline_id = aosp_pipeline.bridges.list()[0].downstream_pipeline["id"]
        ci = gl.projects.get(ANDROID_CI)
        pipeline = get_pipeline(ci, pipeline_id)
        if pipeline is None:
            logging.error("Unknown pipeline id: %d", pipeline_id)
            sys.exit(1)
        ref = ci_get_repo_manifest_sha(pipeline, console)
    else:
        ref = ref2sha(project, ref)

    # Get all Qorvo manifest
    manifests = {}
    try:
        for node in project.repository_tree(
            recursive=True,
            ref=sha.get(project.path_with_namespace, ref),
            iterator=True,
        ):
            if not node["type"] == "blob":
                continue
            if not node["name"].endswith("manifest-qorvo.xml"):
                continue
            console.log(f'Found manifest [green]{node["path"]}[/]')
            content = project.repository_raw_blob(node["id"])
            manifest = RepoManifest(node["path"], content, project)
            manifests[node["path"]] = manifest
    except GitlabGetError as exc:
        logging.error(
            "%s: %s [HTTP %d]",
            repository,
            exc.error_message,
            exc.response_code,
            extra={
                # "markup": True,
                "highlighter": None,
            },
        )
        sys.exit(1)

    # Parse manifest
    # Must be parsed all together to handle 'include' tag
    for manifest in manifests.values():
        manifest.parse(manifests)

    if pipeline_id:
        logging.info("Download pipeline [link=URL]%s[/link]", pipeline.web_url)
        override_manifests = {}
        for job in pipeline.jobs.list(iterator=True):
            if not job.name.startswith("rev-locked-manifest:"):
                continue

            job = ci.jobs.get(job.id)
            wait_for_job(job, console)
            xmlfile = job.name[22:-1]
            logging.info("Job %d: manifest=[magenta]%s[/magenta]", job.id, xmlfile)
            if xmlfile not in manifests:
                logging.info("Skipping manifest=[magenta]%s[/magenta]", xmlfile)
                continue

            for artifact in job.attributes["artifacts"]:
                if artifact["file_type"] == "archive":
                    break
            else:
                logging.warn("Job %d (%s) artifact not available", job.id, job.name)
                continue
            zip = ZipFile(io.BytesIO(job.artifacts()))
            for file in zip.infolist():
                if Path(file.filename).name == xmlfile:
                    break
            else:
                logging.error(
                    "pipeline %d: job prepare %d has no manifest",
                    job.pipeline["id"],
                    job.id,
                )
                sys.exit(1)
            local_manifest = zip.extract(file, tempfile.mkdtemp())
            logging.info(
                "Job %d: downloaded manifest=[magenta]%s[/magenta]",
                job.id,
                local_manifest,
            )
            with Path(local_manifest).open("rb") as reader:
                content = reader.read()
            override_manifests[xmlfile] = RepoManifest(xmlfile, content, project)

        for manifest in override_manifests.values():
            manifest.parse(override_manifests)

        all_projects = {}
        for manifest in override_manifests.values():
            all_projects |= manifest.projects
        console.log("override_manifest", all_projects)

        for manifest in manifests.values():
            xmlfile = manifest.path
            if xmlfile not in override_manifests:
                continue
            override_manifest = override_manifests[xmlfile]
            for prj in manifest.projects:
                if prj not in override_manifest.projects:
                    continue
                manifest.projects[prj]["ref"] = override_manifest.projects[prj]["ref"]

    release_projects = {}
    for manifest in manifests.values():
        for prj in manifest.projects.values():
            if "groups" not in prj:
                continue
            if "release" not in prj["groups"]:
                continue
            url = prj["url"]
            prj_ref = prj["ref"]
            if url in release_projects:
                if prj_ref != release_projects[url]:
                    logging.warning(
                        "[magenta]%s[/]: different revisions"
                        " [cyan]%s[/] vs. [cyan]%s[/]",
                        urlparse(url).path[1:],
                        release_projects[url],
                        prj_ref,
                    )
                    continue
            release_projects[url] = prj_ref
    if not release_projects:
        console.log("Projects to release empty: nothing to do")
        return

    console.log("Projects to release:", release_projects)
    for url, prj_ref in release_projects.items():
        prj_path = urlparse(url).path[1:]
        if prj_path.endswith(".git"):
            prj_path = prj_path[:-4]
        if prj_path == repository:
            continue
        prj = gl.projects.get(prj_path)
        create_release_branch(
            prj, release_branch, sha.get(prj_path, prj_ref), force, dry_run, console
        )
    if pipeline_id:
        create_release_branch(
            ci,
            release_branch,
            sha.get(ci.path_with_namespace, pipeline.sha),
            force,
            dry_run,
            console,
        )

    exists_release_branch(project, release_branch, force, dry_run, console)
    data = {
        "branch": release_branch,
        "start_sha": sha.get(repository, ref),
        "commit_message": f"{obj['issue']} | {obj['commit']}",
        "actions": [],
    }
    for manifest_path, manifest in manifests.items():
        data["actions"].append(
            {
                "action": "update",
                "file_path": manifest_path,
                "content": manifest.update_projects_revision(release_branch),
            }
        )
    if dry_run:
        console.log(
            f"[orange_red1]TODO[/]: create commit in [cyan]{project.path_with_namespace}[/]"
            f" on branch [green]{release_branch}[/] to update"
            f" {', '.join([action['file_path'] for action in data['actions']])}"
        )
    else:
        try:
            commit_obj = project.commits.create(data)
            console.log(f"[link=URL]{commit_obj.web_url}[/link]")
        except GitlabCreateError as exc:
            logging.warning(
                "%s: %s [HTTP %d]",
                project.path_with_namespace,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )


@release.command("tools", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
def tools_release(obj) -> None:
    """Create release branch on tools repositories"""
    console = obj["console"]
    gl = obj["gl"]
    release_branch = obj["release_branch"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    for name, repository in TOOLS.items():
        try:
            project = gl.projects.get(repository)
        except GitlabGetError as exc:
            logging.error(
                "%s: %s [HTTP %d]",
                repository,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            sys.exit(1)

        ref = project.default_branch
        ref = ref2sha(project, ref)
        create_release_branch(
            project,
            release_branch,
            ref,
            force,
            dry_run,
            console,
        )


@tag.command("tools", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
def tools_tag(obj) -> None:
    """Create tag on tools repositories"""
    console = obj["console"]
    gl = obj["gl"]
    tag = obj["tag"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    for name, repository in TOOLS.items():
        try:
            project = gl.projects.get(repository)
        except GitlabGetError as exc:
            logging.error(
                "%s: %s [HTTP %d]",
                repository,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            sys.exit(1)

        ref = project.default_branch
        ref = ref2sha(project, ref)
        if dry_run:
            console.log(
                f"[orange_red1]TODO[/]: create tag {tag}"
                f" in [magenta]{project.path_with_namespace}[/]"
                f" on sha [green]{ref}[/]"
            )
        else:
            try:
                tag_obj = project.tags.create(dict(tag_name=tag, ref=ref))
                console.log(
                    f"tag {tag_obj.name} created"
                    f" in [magenta]{project.path_with_namespace}[/]"
                    f" on sha [green]{tag_obj.commit['id']}[/]"
                )
            except GitlabCreateError as exc:
                logging.warning(
                    "%s: %s [HTTP %d]",
                    project.path_with_namespace,
                    exc.error_message,
                    exc.response_code,
                    extra={
                        # "markup": True,
                        "highlighter": None,
                    },
                )


@release.command("sdk", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
def sdk_release(obj) -> None:
    """Create release branch on SDK repositories"""
    console = obj["console"]
    gl = obj["gl"]
    release_branch = obj["release_branch"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    for name, repository in SDK.items():
        try:
            project = gl.projects.get(repository)
        except GitlabGetError as exc:
            logging.error(
                "%s: %s [HTTP %d]",
                repository,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            sys.exit(1)

        ref = project.default_branch
        ref = ref2sha(project, ref)
        create_release_branch(
            project,
            release_branch,
            ref,
            force,
            dry_run,
            console,
        )


@tag.command("sdk", context_settings=CONTEXT_SETTINGS)
@click.pass_obj
def sdk_tag(obj) -> None:
    """Create tag on SDK repositories"""
    console = obj["console"]
    gl = obj["gl"]
    tag = obj["tag"]
    force = obj["force"]
    dry_run = obj["dry-run"]

    for name, repository in SDK.items():
        try:
            project = gl.projects.get(repository)
        except GitlabGetError as exc:
            logging.error(
                "%s: %s [HTTP %d]",
                repository,
                exc.error_message,
                exc.response_code,
                extra={
                    # "markup": True,
                    "highlighter": None,
                },
            )
            sys.exit(1)

        ref = project.default_branch
        ref = ref2sha(project, ref)
        if dry_run:
            console.log(
                f"[orange_red1]TODO[/]: create tag {tag}"
                f" in [magenta]{project.path_with_namespace}[/]"
                f" on sha [green]{ref}[/]"
            )
        else:
            try:
                tag_obj = project.tags.create(dict(tag_name=tag, ref=ref))
                console.log(
                    f"tag {tag_obj.name} created"
                    f" in [magenta]{project.path_with_namespace}[/]"
                    f" on sha [green]{tag_obj.commit['id']}[/]"
                )
            except GitlabCreateError as exc:
                logging.warning(
                    "%s: %s [HTTP %d]",
                    project.path_with_namespace,
                    exc.error_message,
                    exc.response_code,
                    extra={
                        # "markup": True,
                        "highlighter": None,
                    },
                )


def main():
    cli(obj={})  # pylint: disable=no-value-for-parameter


if __name__ == "__main__":
    sys.exit(main())
