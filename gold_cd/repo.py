import re
import xml.etree.ElementTree as ET
from urllib.parse import urljoin
from urllib.parse import urlparse

from .manifest import Manifest

__all__ = ["RepoManifest"]


class RepoManifest(Manifest):
    """manages the repo manifest file"""

    def update_projects_revision(self, revision: str) -> str:
        result = []
        for line in self._content.decode().split("\n"):
            if re.match(r"\s+\<project", line):
                match = re.search(r'groups="([^"]*)"', line)
                if match and "release" in match.group(1):
                    result.append(
                        re.sub(r'\brevision="([^"]*)"', f'revision="{revision}"', line)
                    )
                    continue
            result.append(line)
        return "\n".join(result)

    def parse(self, manifests: dict | None = None):
        if self.projects is not None:
            return
        self._remotes = {}
        self.projects = {}
        tree = ET.ElementTree(ET.fromstring(self._content))
        root = tree.getroot()
        for tag in root:
            match tag.tag:
                case "include":
                    if manifests is None:
                        raise Exception('Error: unable to handle "include" tag')
                    include_manifest = manifests[tag.attrib["name"]]
                    if include_manifest.projects is None:
                        include_manifest.parse(manifests)
                    self._remotes = self._remotes | include_manifest._remotes
                    self.projects = self.projects | include_manifest.projects
                case "remove-project":
                    project_name = tag.attrib["name"]
                    if project_name in self.projects:
                        del self.projects[project_name]
                case "project":
                    project_name = tag.attrib["name"]
                    if project_name in self.projects:
                        raise Exception(f"Error: duplicate {project_name}")
                    url = urljoin(
                        self._remotes[tag.attrib["remote"]],
                        project_name,
                    )
                    self.projects[project_name] = {
                        "ref": tag.attrib["revision"],
                        "url": url,
                    }
                    if "groups" in tag.attrib:
                        groups = tag.attrib["groups"].split(",")
                        self.projects[project_name]["groups"] = groups
                case "remote":
                    url = urlparse(tag.attrib["fetch"])
                    if not url.scheme:
                        tag.attrib["fetch"] = urljoin(
                            self._repo.web_url, tag.attrib["fetch"]
                        )
                    if tag.attrib["fetch"][-1] != "/":
                        tag.attrib["fetch"] += "/"
                    self._remotes[tag.attrib["name"]] = tag.attrib["fetch"]
